import android.content.BroadcastReceiver;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;
import android.util.Log;

/**
 * Created by Dominik on 17-Apr-16.
 */
public class BTBCReceiver extends BroadcastReceiver {

    private BluetoothAdapter mBluetoothAdapter;

    private BtLeScanCallback mBtLeScanCallback;

    private static boolean alert_sent = false;

    public BtAdapterBroadcastReceiver(BluetoothAdapter bluetoothAdapter){
        mBluetoothAdapter = bluetoothAdapter;

        mBtLeScanCallback = new BtLeScanCallback();

        if(mBluetoothAdapter.getState() == BluetoothAdapter.STATE_ON){
            mBluetoothAdapter.startLeScan(mBtLeScanCallback);
        }
        else {

        }
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();

        if(action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)){
            final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
            switch(state){
                case BluetoothAdapter.STATE_OFF:
                    break;
                case BluetoothAdapter.STATE_TURNING_OFF:
                    mBluetoothAdapter.stopLeScan(mBtLeScanCallback);
                    break;
                case BluetoothAdapter.STATE_ON:
                    mBluetoothAdapter.startLeScan(mBtLeScanCallback);
                    break;
                case BluetoothAdapter.STATE_TURNING_ON:
                    break;
            }
        }
    }

    private static class BtLeScanCallback implements BluetoothAdapter.LeScanCallback {

        public BtLeScanCallback(){
        }
        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
            if(device.getAddress().equalsIgnoreCase("1C:BA:8C:1F:86:5A")) {
                Log.d("[scanRecord]", bytesToHex(scanRecord));
                if(!alert_sent && bytesToHex(scanRecord).equalsIgnoreCase("0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000")) {
                    sendSMS("01706631881", "Fabi ist doof =P");
                    alert_sent = true;
                }
            }
        }
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    private void sendSMS(String phoneNumber, String message) {
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, null, null);
    }
}
